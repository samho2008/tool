# Tor Browser

下载地址：[https://www.torproject.org/download/](https://www.torproject.org/download/)



## 11.5.6

### Android

- tor-browser-11.5.6-android-aarch64-multi.apk: [https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-aarch64-multi.apk](https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-aarch64-multi.apk)

- tor-browser-11.5.6-android-armv7-multi.apk: [https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-armv7-multi.apk](https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-armv7-multi.apk)

- tor-browser-11.5.6-android-x86_64-multi.apk: [https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-x86_64-multi.apk](https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-x86_64-multi.apk)

- tor-browser-11.5.6-android-x86-multi.apk: [https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-x86-multi.apk](https://dist.torproject.org/torbrowser/11.5.6/tor-browser-11.5.6-android-x86-multi.apk)



## 11.5.7

### Windows

- torbrowser-install-win64-11.5.7_en-US.exe: [https://dist.torproject.org/torbrowser/11.5.7/torbrowser-install-win64-11.5.7_en-US.exe](https://dist.torproject.org/torbrowser/11.5.7/torbrowser-install-win64-11.5.7_en-US.exe)
- torbrowser-install-win64-11.5.7_zh-CN.exe: [https://dist.torproject.org/torbrowser/11.5.7/torbrowser-install-win64-11.5.7_zh-CN.exe](https://dist.torproject.org/torbrowser/11.5.7/torbrowser-install-win64-11.5.7_zh-CN.exe)



### macOS

- TorBrowser-11.5.7-osx64_en-US.dmg: [https://dist.torproject.org/torbrowser/11.5.7/TorBrowser-11.5.7-osx64_en-US.dmg](https://dist.torproject.org/torbrowser/11.5.7/TorBrowser-11.5.7-osx64_en-US.dmg)
- TorBrowser-11.5.7-osx64_zh-CN.dmg: [https://dist.torproject.org/torbrowser/11.5.7/TorBrowser-11.5.7-osx64_zh-CN.dmg](https://dist.torproject.org/torbrowser/11.5.7/TorBrowser-11.5.7-osx64_zh-CN.dmg)



### Linux

- tor-browser-linux64-11.5.7_en-US.tar.xz: [https://dist.torproject.org/torbrowser/11.5.7/tor-browser-linux64-11.5.7_en-US.tar.xz](https://dist.torproject.org/torbrowser/11.5.7/tor-browser-linux64-11.5.7_en-US.tar.xz)

- tor-browser-linux64-11.5.7_zh-CN.tar.xz: [https://dist.torproject.org/torbrowser/11.5.7/tor-browser-linux64-11.5.7_zh-CN.tar.xz](https://dist.torproject.org/torbrowser/11.5.7/tor-browser-linux64-11.5.7_zh-CN.tar.xz)

