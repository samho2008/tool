### JDK 8

Oracle 从 JDK 8u211 这个版本开始修改了 Oracle JDK License 协议，对商用进行收费，个人学习和开发仍然免费。**所以，JDK 8u202 是最后一个商用免费的版本。**

注意：JDK 的版本号不是按顺序的，8u202 的下一个版本号是 8u211



同时，也提供了免安装版本【jdk1.8.0_202.7z】，从安装好的软件中提取出来的。



Java SE 8 Archive Downloads (JDK 8u202 and earlier)：[https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html)

Java SE 8 Archive Downloads (JDK 8u211 and later)：[https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html](https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html)

