# ShadowsocksR

## Chrome 浏览器插件

下载地址：[https://github.com/atrandys/trojan/releases/download/1.0.0/SwitchyOmega_Chromium.crx](https://github.com/atrandys/trojan/releases/download/1.0.0/SwitchyOmega_Chromium.crx)



## Windows SSR 客户端

下载地址：[https://github.com/shadowsocksr-backup/shadowsocksr-csharp/releases](https://github.com/shadowsocksr-backup/shadowsocksr-csharp/releases)



## Mac SSR 客户端

下载地址：[https://github.com/shadowsocksr-backup/ShadowsocksX-NG/releases](https://github.com/shadowsocksr-backup/ShadowsocksX-NG/releases)



## 安卓 SSR 客户端

下载地址：[https://github.com/shadowsocksr-backup/shadowsocksr-android/releases/download/3.4.0.8/shadowsocksr-release.apk](https://github.com/shadowsocksr-backup/shadowsocksr-android/releases/download/3.4.0.8/shadowsocksr-release.apk)



## 全平台 SS/SSR 客户端下载汇总

下载地址：[http://www.mediafire.com/folder/sfqz8bmodqdx5/shadowsocks%E7%9B%B8%E5%85%B3%E5%AE%A2%E6%88%B7%E7%AB%AF](http://www.mediafire.com/folder/sfqz8bmodqdx5/shadowsocks%E7%9B%B8%E5%85%B3%E5%AE%A2%E6%88%B7%E7%AB%AF)

