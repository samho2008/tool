# VC++ 运行库

下载地址：[https://docs.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist](https://docs.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist)

用于解决运行程序时提示【由于找不到 MSVCR110.dll，无法继续执行代码。重新安装程序可能会解决此问题。】的报错。

其中，MSVCR 表示 MicroSoft VC++ Runtime，110 表示 vc++11 的运行库。