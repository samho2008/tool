## Visual Studio 2015, 2017, 2019, and 2022

后面微软把几个版本打包到一起了，版本对应关系如下，17 向下兼容其他版本：

Visual Studio C++ 2022 aka VS17. 

Visual Studio C++ 2019 aka VS16. 

Visual Studio C++ 2017 aka VC15.

Visual Studio C++ 2015 aka VC14.

- ARM64：[https://aka.ms/vs/17/release/vc_redist.arm64.exe](https://aka.ms/vs/17/release/vc_redist.arm64.exe)
- X86：[https://aka.ms/vs/17/release/vc_redist.x86.exe](https://aka.ms/vs/17/release/vc_redist.x86.exe)
- X64：[https://aka.ms/vs/17/release/vc_redist.x64.exe](https://aka.ms/vs/17/release/vc_redist.x64.exe)



> 参考文献：
>
> [https://www.apachelounge.com/download/](https://www.apachelounge.com/download/)

