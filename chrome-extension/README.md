# Chrome 谷歌浏览器扩展程序

| 扩展程序名              | 扩展程序 ID                      | 描述                                                         | 下载地址                                                     | Chrome 应用商店下载地址                                      |
| ----------------------- | -------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Charset                 | oenllhgkiiljibhfagbfogdbchhdchml | 修改网站的默认编码                                           | [https://github.com/jinliming2/Chrome-Charset/releases](https://github.com/jinliming2/Chrome-Charset/releases) | [https://chrome.google.com/webstore/detail/charset/oenllhgkiiljibhfagbfogdbchhdchml](https://chrome.google.com/webstore/detail/charset/oenllhgkiiljibhfagbfogdbchhdchml) |
| crxMouse Chrome™ 手势   | jlgkpaicikihijadgifklkbpdajbkhjo | 原名:Gestures for Chrome(TM)汉化版.方便,快捷,充分发掘鼠标的所有操作.功能包括:鼠标手势,超级拖曳,滚轮手势,摇杆手势,平滑滚动,标签页列表等. | [https://crxmouse.com/](https://crxmouse.com/)               | [https://chrome.google.com/webstore/detail/crxmouse-chrome-gestures/jlgkpaicikihijadgifklkbpdajbkhjo](https://chrome.google.com/webstore/detail/crxmouse-chrome-gestures/jlgkpaicikihijadgifklkbpdajbkhjo) |
| JSONView（新版本）      | gmegofmjomhknnokphhckolhcffdaihd | 在浏览器中查看JSON 文件.                                     | [https://www.crx4chrome.com/crx/134083/](https://www.crx4chrome.com/crx/134083/) | [https://chrome.google.com/webstore/detail/jsonview/gmegofmjomhknnokphhckolhcffdaihd](https://chrome.google.com/webstore/detail/jsonview/gmegofmjomhknnokphhckolhcffdaihd) |
| JSONView（旧版本）      | eoconaeimioiejmaedihdfanoikohonb | （本项目自 2013 年开始已废弃，插件中部分功能将在 2023 年移除，使用新项目）Validate and view JSON documents | [https://github.com/samho2008/JSONView-for-Chrome](https://github.com/samho2008/JSONView-for-Chrome) |                                                              |
| Ghelper（谷歌上网助手） | cieikaeocafmceoapfogpffaalkncpkc | 专门为科研、外贸、跨境电商、海淘人员、开发人员服务的上网加速工具，chrome内核浏览器专用!可以解决chrome扩展无法自动更新的问题，同时可>以访问谷歌google搜索，gmail邮箱，google+等谷歌产品。可以用于反向代理 | Ghelper 官网下载地址：[http://googlehelper.net/](http://googlehelper.net/) |                                                              |
| 侧边翻译                | bocbaocobfecmglnmeaeppambideimao | 使用教程：[https://github.com/EdgeTranslate/EdgeTranslate/blob/master/docs/wiki/zh_CN/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.md](https://github.com/EdgeTranslate/EdgeTranslate/blob/master/docs/wiki/zh_CN/%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E.md) | 下载地址：[https://github.com/EdgeTranslate/EdgeTranslate/releases](https://github.com/EdgeTranslate/EdgeTranslate/releases) | [https://chrome.google.com/webstore/detail/edge-translate/bocbaocobfecmglnmeaeppambideimao](https://chrome.google.com/webstore/detail/edge-translate/bocbaocobfecmglnmeaeppambideimao) |

> 安装完 JSONView 插件后打开以下 JSON 格式网址来查看插件是否生效：
>
> [http://jsonview.com/example.json](http://jsonview.com/example.json)
>
> [https://api.pictureknow.com/api/v1/version](https://api.pictureknow.com/api/v1/version)



