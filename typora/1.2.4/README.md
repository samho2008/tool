# Typora 1.2.4 破解版

## 具体步骤

1、双击安装【typora-setup-x64-1.2.4.exe】，不要修改默认安装路径，勾选在桌面创建快捷方式【shortcut】。

2、打开【C:\Program Files\Typora\resources】文件夹，把破解后的【app.asar】文件替换进去。

3、重新启动 Typora，点击【激活 Typora】，邮箱地址随便填写，比如：abc@example.com，序列号填【E8Q9Y5-KXMTL5-7578SL-4S5XKS】，填写完成后点击激活。

4、看到【Typora 已激活】，说明破解成功了。

